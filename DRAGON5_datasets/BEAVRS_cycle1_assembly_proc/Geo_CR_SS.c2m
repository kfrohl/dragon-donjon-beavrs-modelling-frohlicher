*DECK Geo_AIC_SS
*----
*  Name          : Geo_AIC_SS.c2m
*  Type          : DRAGON procedure
*  Use           : Geometry generation for Self-Shielding Calculation
*                  with 32 fuel regions and AIC
*  Author        : A. Canbakan
*
*  Procedure called as: 
*
*GEOSS := Geo_AIC_SS :: <<Rcomb1>> <<Rcomb2>> <<Rcomb3>> <<Rcomb4>>
*                   <<R_int_TG>> <<R_ext_TG>> <<R_int_TI>> 
*                   <<R_ext_TI>> <<R_int_G>> <<R_ext_G>>
*                   <<Raic1>> <<Raic2>> <<Raic3>> <<Raic4>>
*                   <<R_int_CR>> <<R_ext_CR>>
*                   <<Cote>> <<CoteL>> ;
*
*  Input data   :
*    Rcomb1     :  50/100 of outer radius of fuel (cm)
*    Rcomb2     :  80/100 of outer radius of fuel (cm)
*    Rcomb3     :  95/100 of outer radius of fuel (cm)
*    Rcomb4     : 100/100 of outer radius of fuel (cm)
*    R_int_TG   : Inner radius of cladding of guide tube (cm)
*    R_ext_TG   : Outer radius of cladding of guide tube (cm)
*    R_int_TI   : Inner radius of cladding of instrument guide tube (cm)
*    R_ext_TI   : Outer radius of cladding of instrument guide tube (cm)
*    R_int_G    : Inner radius of cladding of fuel tube (cm)
*    R_ext_G    : Outer radius of cladding of fuel tube (cm)
*    Raic1      :  50/100 of outer radius of AIC rod (cm)
*    Raic2      :  80/100 of outer radius of AIC rod (cm)
*    Raic3      :  95/100 of outer radius of AIC rod (cm)
*    Raic4      : 100/100 of outer radius of AIC rod (cm)
*    R_int_CR  : Inner radius of cladding of AIC rod (cm)
*    R_ext_CR  : Outer radius of cladding of AIC rod (cm)
*    Cote       : Lattice pitch (cm)
*    CoteL      : Lattice pitch + Water space (cm)
*
*  Output data  :
*    GEOSS      : Geometry for Self-Shielding Calculation

PARAMETER  GEOSS  ::  
       EDIT 0 
           ::: LINKED_LIST GEOSS  ; 
   ;
*----
*  Modules used in this procedure
*----
MODULE  GEO: END: ;

*----
*  Input data recovery
*----
*  
STRING CR ;
:: >>CR<< ;
REAL Rcomb1       Rcomb2       Rcomb3       Rcomb4     ;
:: >>Rcomb1<<   >>Rcomb2<<   >>Rcomb3<<   >>Rcomb4<<   ;
REAL R_int_TG     R_ext_TG     R_int_TI     R_ext_TI   ;
:: >>R_int_TG<< >>R_ext_TG<< >>R_int_TI<< >>R_ext_TI<< ;
REAL R_int_G      R_ext_G   ;
:: >>R_int_G<<  >>R_ext_G<< ;
REAL Raic1     Raic2     Raic3     Raic4     R_ext_CR   ;
:: >>Raic1<< >>Raic2<< >>Raic3<< >>Raic4<< >>R_ext_CR<< ;
REAL Rb4c1     Rb4c2     Rb4c3     Rb4c4   ;
:: >>Rb4c1<< >>Rb4c2<< >>Rb4c3<< >>Rb4c4<< ;
REAL Cote         CoteL      ;
:: >>Cote<<     >>CoteL<<    ;

GEOSS := GEO: :: CAR2D 9 9
  EDIT 0
  X- DIAG X+ REFL
  Y- SYME Y+ DIAG
  CELL TI C1 C1 R1 C1 C1 R2 C4 C6
          C2 C2 C1 C2 C2 C1 C2 C6
             C2 C1 C2 C2 C1 C2 C6
                R1 C1 C1 R2 C4 C6
                   C2 C1 C1 C2 C6
                      R2 C1 C3 C6
                         C2 C3 C6
                            C5 C7
                               C8

  MERGE 11 1  1 10  1  1  9  4  6
           2  2  1  2  2  1  2  6
              2  1  2  2  1  2  6
                10  1  1  9  4  6
                    2  1  1  2  6
                       9  1  3  6
                          2  3  6
                             5  7
                                8

* T2 -> 9 , T1 ->  10 , TI -> 11

  TURN  A  A  E  A  A  E  A  A  A
           A  E  F  A  E  D  A  A
              C  B  G  C  H  G  A
                 A  G  C  A  G  A
                    A  B  D  A  A
                       A  A  A  A
                          A  G  A
                             A  A
                                A


  ::: C1 := GEO: CARCEL 5
         MESHX 0.0 <<Cote>>
         MESHY 0.0 <<Cote>>
         RADIUS 0.0 <<Rcomb1>> <<Rcomb2>> <<Rcomb3>> <<Rcomb4>>
                <<R_ext_G>>
         MIX  34 35 36 37    4 67
  ;
  ::: C2 := GEO: C1
         MIX 38 39 40 41    4 67
  ;
  ::: C3 := GEO: C1
         MIX 42 43 44 45     4 67
  ;
  ::: C4 := GEO: C1
         MIX 46 47 48 49    4 67
  ;
  ::: C5 := GEO: C1
         MIX 50 51 52 53    4 67
  ;
  ::: C6 := GEO: CARCEL 5
         MESHX 0.0 <<CoteL>>
         MESHY 0.0 <<Cote>>
         RADIUS 0.0 <<Rcomb1>> <<Rcomb2>> <<Rcomb3>> <<Rcomb4>>
                <<R_ext_G>>
         MIX 54 55 56 57    4 68
  ;
  ::: C7 := GEO: C6
         MIX 58 59 60 61    4 68
  ;
  ::: C8 := GEO: CARCEL 5
         MESHX 0.0 <<CoteL>>
         MESHY 0.0 <<CoteL>>
         RADIUS 0.0 <<Rcomb1>> <<Rcomb2>> <<Rcomb3>> <<Rcomb4>>
                <<R_ext_G>>
         MIX 62 63 64 65    4 68
  ;
  ::: R2 := GEO: CARCEL 7
         MESHX 0.0 <<Cote>>
         MESHY 0.0 <<Cote>>
         RADIUS 0.0 <<Raic1>> <<Raic2>> <<Raic3>> <<Raic4>> 
         <<R_ext_CR>> <<R_int_TG>> <<R_ext_TG>>
         MIX 14 15 16 17   5 1 3 67
  ;
  ::: R1 := GEO: R2
         MIX 10 11 12 13   5 1 3 67
  ;
  ::: TI := GEO: CARCEL 2
         MESHX 0.0 <<Cote>>
         MESHY 0.0 <<Cote>>
         RADIUS 0.0 <<R_int_TI>> <<R_ext_TI>>
         MIX 66 2 67
  ;
;

IF CR 'B4C' = THEN
  GEOSS := GEO: GEOSS ::
    ::: R2 := GEO: CARCEL 7
           MESHX 0.0 <<Cote>>
           MESHY 0.0 <<Cote>>
           RADIUS 0.0 <<Rb4c1>> <<Rb4c2>> <<Rb4c3>> <<Rb4c4>> 
           <<R_ext_CR>> <<R_int_TG>> <<R_ext_TG>>
           MIX 22 23 24 25   6 1 3 67
    ;
    ::: R1 := GEO: R2
           MIX 18 19 20 21   6 1 3 67
    ;
  ;

ENDIF ;

END: ;
QUIT .
