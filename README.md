# General informations
Datasets used to model BEAVRS cycle1 HZP startup with DRAGON5 and DONJON5.  
Initially intended for full depletion calculation of cycle 1, datasets have not been tested after t=0.  
These datasets were written for DRAGON and DONJON version 5.0.5 ev1761.  



# Draglib used for DRAGON lattice calculations
Name : draglibJeff3p1p1SHEM295_2012  
Energy mesh : SHEM295  
Nuclear data evaluations : JEFF3.1.1  
## Files
[draglib_shem295_jeff311.tar.gz.aa]  
[draglib_shem295_jeff311.tar.gz.ab]  
[draglib_shem295_jeff311.tar.gz.ac]  
[draglib_shem295_jeff311.tar.gz.ad]  
[draglib_shem295_jeff311.tar.gz.ae]  
[draglib_shem295_jeff311.tar.gz.af]  
[draglib_shem295_jeff311.tar.gz.ag]  
[draglib_shem295_jeff311.tar.gz.ah]  
[draglib_shem295_jeff311.tar.gz.ai]  
[draglib_shem295_jeff311.tar.gz.aj]  
[draglib_shem295_jeff311.tar.gz.ak]  
[draglib_shem295_jeff311.tar.gz.al]  
[draglib_shem295_jeff311.tar.gz.am]  
[draglib_shem295_jeff311.tar.gz.an]  
[draglib_shem295_jeff311.tar.gz.ao]  

Use   
cat draglib_shem295_jeff311.tar.gz.* | tar xzvf -  
to retrieve the full file.  


# Multicompos used in DONJON calculation
## Reflector
See https://github.com/IRSN/SalinoPhD/releases/tag/ANE2021
## Homogenization case A
[_ACompo_0BA_1_6_flux]  
[_ACompo_0BA_2_4_flux]  
[_ACompo_0BA_3_1_flux]  
[_ACompo_6BA_3_1_flux]  
[_ACompo_12BA_2_4_flux]  
[_ACompo_15BA_3_1_flux]  
[_ACompo_16BA_2_4_flux]  
[_ACompo_16BA_3_1_flux]  
[_ACompo_20BA_3_1_flux]  
## Homogenization case B  
[_ACompo_0BA_1_6_flux]  
[_ACompo_0BA_2_4_flux]  
[_ACompo_0BA_3_1_flux]  
[_ACompo_6BA_3_1_ass]  
[_ACompo_12BA_2_4_flux]  
[_ACompo_15BA_3_1_ass]  
[_ACompo_16BA_2_4_flux]  
[_ACompo_16BA_3_1_flux]  
[_ACompo_20BA_3_1_flux]  
## Homogenization case C  
[_ACompo_0BA_1_6_flux]  
[_ACompo_0BA_2_4_flux]  
[_ACompo_0BA_3_1_flux]  
[_ACompo_6BA_1]  
[_ACompo_6BA_2]  
[_ACompo_12BA_2_4_flux]  
[_ACompo_15BA_1]  
[_ACompo_15BA_2]  
[_ACompo_15BA_3]  
[_ACompo_16BA_2_4_flux]  
[_ACompo_16BA_3_1_flux]  
[_ACompo_20BA_3_1_flux]  
